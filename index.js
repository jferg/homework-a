(function () {
  /**
   * Get handles to our various inputs and buttons.
   *
   * The `formInputs` object contains each form input in the document,
   * keyed by name, so we don't have to create individual variables for
   * each input.
   */
  var formInputs = Array.from(
    document.querySelectorAll("input, select")
  ).reduce((accum, current) => {
    accum[current.name] = current;
    return accum;
  }, {});

  /** 
   * Buttons are inconsistently identified, so have to jump through different
   * hoops to get a handle to them.
   */
  var addButton = document.getElementsByClassName("add");
  if (addButton) {
    addButton = addButton[0];
  }

  var submitButton = document.querySelector("button[type=submit]");

  var householdContainer = document.getElementsByClassName("household")[0];
  var currentMembers = [];

  /**
   * These are the rules that determine whether the values input into a form field are valid.
   * It is a map of the field name to a function that determines whether the input in that field
   * is valid, and returns true if it is valid, an error message if not.
   */
  var rules = {
    age: (val) => (val && val > 0) || "Must be a number greater than 0.",
    rel: (val) =>
      (val !== undefined && val !== "") || "Must be selected."
  };

  /**
   * Checks the inputs in the form against the provided rules.
   * Returns an object containing a mapping of input field name to
   * either true if the input is valid or an error message if it is
   * not.
   */
  var formIsValid = function () {
    var valid = {};

    Object.keys(formInputs).forEach((f) => {
      if (typeof rules[f] === "function") {
        valid[f] = rules[f](formInputs[f].value);
      }
    });

    return valid;
  };

  /**
   * Looks at the field validity object and sets or clears errors on 
   * fields, as appropriate.  Returns true if all fields are valid.
   * 
   * @param fieldValidity 
   */
  var applyFieldErrors = function (fieldValidity) {
    let isValid = true;

    // Loop through all the fields.  Since all the inputs are properly wrapped
    // in labels, we're actually looping through the labels.  
    Object.keys(fieldValidity).forEach((k) => {
      var label = document.querySelector(`label[for="${k}"]`);
      var currentErrorNode = document.querySelector(`error[for="${k}"]`);
      if (label) {
        if (fieldValidity[k] !== true) {
          if (!currentErrorNode) {
            currentErrorNode = document.createElement('error');
            currentErrorNode.setAttribute('for', k);
            currentErrorNode.setAttribute('id', `error-${k}`);
            currentErrorNode.setAttribute('aria-live', 'polite');
            label.setAttribute('aria-errormessage', `error-${k}`);
            label.appendChild(currentErrorNode);
          } 

          currentErrorNode.innerText = fieldValidity[k];
          label.setAttribute('aria-invalid', 'true');

          isValid = false;

        } else {
          if (currentErrorNode) {
            label.removeChild(currentErrorNode);
          }
          if (label.attributes.getNamedItem("aria-errormessage")) {
            label.attributes.removeNamedItem("aria-errormessage");
          }
          if (label.attributes.getNamedItem("aria-invalid")) {
            label.attributes.removeNamedItem("aria-invalid");
          }
        }
      }
    });

    return isValid;
  }

  /**
   * Calls applyFieldErrors to set errors messages, then sets button
   * states based on whether or not there are errors in the form and/or
   * whether the household has members.
   *  
   * @param {*} fieldValidity 
   */
  var setFormState = function (fieldValidity) {

    // Check fields, and toggle button states based on results.
    if (applyFieldErrors(fieldValidity)) {
      enableAdd();
    } else {
      disableAdd();
      disableSubmit();
    }

    // If we have at least one household member, enable the submit button.
    if (currentMembers.length > 0) {
      enableSubmit();
    } else { 
      disableSubmit();
    }
  };

  /**
   * These are just toggles for the submission buttons.  Might be 
   * overkill to create individual functions but trying to be 
   * complete.
   */
  var disableAdd = function () {
    addButton.disabled = true;
  };

  var enableAdd = function () {
    addButton.disabled = false;
  };

  var disableSubmit = function () {
    submitButton.disabled = true;
  };

  var enableSubmit = function () {
    submitButton.disabled = false;
  };

  /**
   * Need to at least do some things to make it look a little nicer.  We'll
   * just apply a stylesheet rather than trying to get too fancy.
   */
  var applyStyles = function () {
    var header = document.head;
    var styleLink = document.createElement("link");
    styleLink.type = "text/css";
    styleLink.rel = "stylesheet";
    styleLink.href = "index.css";
    header.appendChild(styleLink);
  };

  /**
   * This registers the input-change event handler on each of the
   * inputs.  It registers a handler for 'onchange' for all inputs,
   * and for 'text' or 'password' inputs it also registers a handler
   * for 'onkeyup.
   *
   * Additionally registers button handlers on the buttons.
   */
  var registerHandlers = function () {
    // Register handlers on input fields.
    Object.keys(formInputs).forEach((i) => {
      formInputs[i].onchange = handleInputChange;
      if (["text", "password"].includes(formInputs[i].type)) {
        formInputs[i].onkeyup = handleInputChange;
      }
    });

    // Register handlers on the buttons.
    addButton.onclick = handleAdd;
    submitButton.onclick = handleSubmit;
  };

  /**
   * Clears the form so we can enter another household member.
   */
  var clearForm = function () {
    document.getElementsByTagName("form")[0].reset();
    setFormState(formIsValid());
  };

  /**
   * Renders the household members into the household div.
   */
  var renderHouseholdMember = function (newMember) {
    var renderedMember = document.createElement("li");
    renderedMember.setAttribute('data-member-number', currentMembers.length);

    var ageEl = document.createElement("div");
    ageEl.className = "age";
    ageEl.innerText = `Age: ${newMember.age}`;

    var relationshipEl = document.createElement("div");
    relationshipEl.className = "relationship";
    relationshipEl.innerText = `Relationship: ${newMember.relationship}`;

    var smokerEl = document.createElement("div");
    smokerEl.className = "smoker";
    smokerEl.innerText = `Is a smoker?: ${newMember.isSmoker ? "Yes" : "No"}`;

    var removeButtonEl = document.createElement("div");
    removeButtonEl.className = "removeButton";
    removeButtonEl.innerText = 'x';

    renderedMember.appendChild(ageEl);
    renderedMember.appendChild(relationshipEl);
    renderedMember.appendChild(smokerEl);
    renderedMember.appendChild(removeButtonEl);

    // Add a handler to remove the member if the remove button is clicked.
    removeButtonEl.addEventListener('click', (ev) => {
      let dataId = ev.target.parentNode.attributes['data-member-number'];
      if (dataId) {
        currentMembers.splice(dataId - 1, 1);
      }
      ev.target.parentNode.remove();
      setFormState(formIsValid());
    })

    householdContainer.appendChild(renderedMember);
  };

  /**
   * Handles clicks of the 'submit' button.
   * 
   * Sets the content of the 'debug' area to the current household members,
   * and shows it.  
   */
  var handleSubmit = function (ev) {
    var debugArea = document.getElementsByClassName('debug')[0];
    debugArea.innerText = JSON.stringify(currentMembers, null, 2);
    debugArea.className = debugArea.className + " visible";

    // Originally I was clearing the form after submit, but after revisiting
    // the requirements I'm commenting that out to meet the "make changes and 
    // resubmit" requirement. 
    // currentMembers = [];
    // clearForm();
    // householdContainer.innerHTML = '';

    ev.preventDefault();
  };

  /**
   * Handles clicks of the 'add' button by checking form validity,
   * then, if valid, adding a new entry to the end of the current household
   * members.
   */
  var handleAdd = function (ev) {
    var newMember = {
      age: formInputs["age"].value,
      relationship: formInputs["rel"].value,
      isSmoker: formInputs["smoker"].checked,
    };
    currentMembers.push(newMember);
    renderHouseholdMember(newMember);
    clearForm();
    ev.preventDefault();
  };

  /**
   * Handles changes to any of the inputs.
   */
  var handleInputChange = function (ev) {
    if (ev && ev.target) {
      setFormState(formIsValid());
    }

    ev.preventDefault();
  };

  /**
   * DocumentReady handler is the only thing we expose to the global namespace.
   */
  handleDocumentReady = function (ev) {
    if (document.readyState === "complete") {
      applyStyles();

      // Calling this here makes sure that the form is in the correct state
      // on page load.
      setFormState(formIsValid());

      registerHandlers();
    }
  };
})();

document.onreadystatechange = handleDocumentReady;
