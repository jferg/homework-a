I probably went a little overboard on the "assignment", but hey, it was fun.  

Some notes: 

* In a real-world code base there would be tests.  At least for some stuff.  
* I've been mostly coding in Typescript lately so going back to pure browser JS was slightly challenging - if I slipped and used any Typescript idiosyncracies that just happened to work here, my apologies.  
* I was unsure if the specification for "the user should be able to make changes and re-submit" implied they should be able to _edit_ existing entries, but I went with the approach of "remove and re-add" if changes were necessary.  
* The default html is missing some a11y tags but I didn't see adding them as being in scope.